import Logo from "./logo.svg?react"
import colors from "tailwindcss/colors"

export default () => (
    <main className="bg-slate-700 text-slate-200 min-h-screen flex">
        <div className="m-auto text-center">
            <Logo className="h-56 m-5" fill={colors.slate["200"]} stroke={colors.slate["200"]}/>
            <div className="font-specialelite text-4xl">coming soon</div>
        </div>
    </main>
)
