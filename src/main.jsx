import ReactDOM from "react-dom/client"
import React from "react"
import Intro from "./Intro"
import "./index.css"

ReactDOM.createRoot(document.getElementById("root")).render(
    <React.StrictMode>
        <Intro/>
    </React.StrictMode>
)
